//  ______ ______ __   __ ______ ______ ______ __        
// /\  ___\\  ___\\ "-.\ \\  ___\\  == \\  __ \\ \       
// \ \ \__ \\  __\\\ \-.  \\  __\_\  __< \  __ \\ \____  
//  \ \_____\\_____\\_\\"\_\\_____\\_\/\_\\_\/\_\\_____\ 
//   \/_____//_____//_/ \/_//_____//_/\/_//_/\/_//_____/ 
//
// --------------
// Order Matters!
// --------------
//
// The MathML parser is entirely sequential. Once it finds all cases of the
// current pattern and combines the different pattern's elements into singular
// entities, the pattern is no longer used in later matching. This means you
// must order patterns correctly to produce results you expect.
//
// -----------------
// Expected Ordering
// -----------------
//
// In order for patterns to match correctly, you should have patterns grouped
// and have those groups ordered in the following way: 
//
// 1. Symbol Code (like variables and Unicode)
// 2. Fraction-Based (like derivatives)
// 3. Over-Under (like sums and integrals)
// 4. Parenthetical Expressions (like square roots or things in parenthesis)
// 5. Generic Fallbacks (like fractions, superscripting, and subscripting)
//
// ------------------------------------------------------------------------------
// Symbol Code
// ------------------------------------------------------------------------------
import _codes.txt
import _Numbers.txt

// Unicodes and MathML codes for operators
import _Unicodes.txt
import _Negatives.txt

e [variable] = mi("e") -> '"e"'
i [variable] = mi("i") -> '"i"'
z [variable] = mi("z") -> '"z"'

bbcapR = mi("&#8477;") -> 'all real numbers'

// Other symbols

// Strikes and Slashes
import _Vocab.txt
import _BordersSlashes.txt

//---------------------------------------------------------------
// Trigonometry-Specific
//---------------------------------------------------------------
import _Trig.txt

// Products and Unions
import _Integrals.txt
import _FunctionsLimits.txt

//Powers and Primes
apostrophe = apostrophe -> 'prime'

// Syntax Quirks
doubleBarSyn = mover(mover(mrow(+) bar) bar) -> '{1} under double bar'
doubleBarSyn = munder(munder(mrow(+) bar) bar) -> '{1} over double bar'
doubleDagger = dagger dagger -> 'double dagger'

// ------------------------------------------------------------------------------
// Fraction-based
// ------------------------------------------------------------------------------
//integral = msub([integral] ?) -> '{1} sub {2} of,'

// Other symbols
infinity = mi("&#8734;") -> 'infinity'
infinity = mn("&#8734;") -> 'infinity'

// ------------------------------------------------------------------------------
// Conditions for arrows based on over or under
// ------------------------------------------------------------------------------
overBothArrow = munder(? [arrow]) -> '{1}, over {2},'
underBothArrow = mover(? [arrow]) -> '{1}, under {2},'
overUnderArrowB = munderover(rightArrow mrow(+) mrow(+)) -> '{2}, above {1}, sub {3},'
overUnderArrowB = munderover(leftArrow mrow(+) mrow(+)) -> '{2}, above {1}, sub {3},'
overUnderArrowB = munderover(bothArrow mrow(+) mrow(+)) -> '{2}, above {1}, sub {3},'
overUnderArrowB = munderover(rightHarpoon mrow(+) mrow(+)) -> '{2}, above {1}, sub {3},'

// ------------------------------------------------------------------------------
// Under-Over, Integrals, Products, and Summations
// ------------------------------------------------------------------------------
backPrimes = mmultiscripts(? ? ? ?) -> '{4} {1}'

// Unders and Overs
munder = munderover(mrow(+) mrow(+) mrow()) -> '{1} sub {2}'

// ------------------------------------------------------------------------------
// Parenthetical Expressions
// ------------------------------------------------------------------------------
absoluteValue = verticalBar + verticalBar -> 'absolute value {2} end absolute'
import _Roots.txt

// ------------------------------------------------------------------------------
// Matrices
// ------------------------------------------------------------------------------

// ------------------------------------------------------------------------------
// Generic Fallbacks, Final Collectors
// ------------------------------------------------------------------------------
import _SoftFractions.txt
import _Dollars.txt
import _Fractions.txt

fencedABS = mfenced<open="|",close="|",separators="|">(mrow(+) mrow(+) mrow(+)) -> 'vertical bar, {1}, vertical bar, {2}, vertical bar, {3}, vertical bar,'
fencedABS = mfenced<open="|",close="|",separators="|">(mrow(+) mrow(+)) -> 'vertical bar, {1}, vertical bar, {2}, vertical bar,'
fencedABS = mfenced<open="|",close="|">(+) -> 'absolute value of, {1}, end absolute,'

import _Fences.txt
import _Final.txt