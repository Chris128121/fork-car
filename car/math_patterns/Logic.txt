//                                                      
//  ,--.      .-'),-----.   ,----.     ,-.-')   .-----.  
//  |  |.-') ( OO'  .-.  ' '  .-./-')  |  |OO) '  .--./  
//  |  | OO )/   |  | |  | |  |_( O- ) |  |  \ |  |('-.  
//  |  |`-' |\_) |  |\|  | |  | .--, \ |  |(_//_) |OO  ) 
// (|  '---.'  \ |  | |  |(|  | '. (_/,|  |_.'||  |`-'|  
//  |      |    `'  '-'  ' |  '--'  |(_|  |  (_'  '--'\  
//  `------'      `-----'   `------'   `--'     `-----'  
//                                                   
// --------------
// Order Matters!
// --------------
//
// The MathML parser is entirely sequential. Once it finds all cases of the
// current pattern and combines the different pattern's elements into singular
// entities, the pattern is no longer used in later matching. This means you
// must order patterns correctly to produce results you expect.
//
// -----------------
// Expected Ordering
// -----------------
//
// In order for patterns to match correctly, you should have patterns grouped
// and have those groups ordered in the following way: 
//
// 1. Symbol Code (like variables and Unicode)
// 2. Fraction-Based (like derivatives)
// 3. Over-Under (like sums and integrals)
// 4. Parenthetical Expressions (like square roots or things in parenthesis)
// 5. Generic Fallbacks (like fractions, superscripting, and subscripting)
// ------------------------------------------------------------------------------
// Symbol Code
// ------------------------------------------------------------------------------
weirdSpace = mi("&#1160;") -> ' '

divideBack = mo("&#247;") -> 'divided by,'
divideBack = mi("\") -> ', back slash,'
divide = mo("/") -> 'forward slash,'

import _codes.txt
import _Numbers.txt
import _Negatives.txt

// Unicodes and MathML codes for operators
//backSlash = divideBack -> ', back slash,'//forwardSlash = divide -> ', forward slash,'

cross = timesCross -> 'cross'
diamond = mi("&#8900;") -> 'diamond'
identical = mi("&#8801;") -> 'triple bars'
plus = mo("+") -> 'plus sign'
summation = mi("&#8721;") -> 'summation sign,'
tilde = mo("&#732;") -> 'tilde'
tilde = mo("~") -> 'tilde'
tilde = mi("~") -> 'tilde'
times = mi("&#8901;") -> 'dot'
times = mi("&#59791;") -> 'dot'
times = times -> 'dot'

// Parentheses, Brackets, Braces, and Bars
closeBrace = mo("}") -> 'close curly bracket'
closeBracket = mo("]") -> 'close square bracket'
closeParen = mo(")") -> 'close parentheses'
leftAngleBracket = mi("&#9001;") -> 'open angle bracket'
leftAngleBracket = mo("&#9001;") -> 'open angle bracket'
openBrace = mo("{") -> 'open curly bracket'
openBracket = mo("[") -> 'open square bracket'
rightAngleBracket = mi("&#9002;") -> 'close angle bracket'
rightAngleBracket = mo("&#9002;") -> 'close angle bracket'
bullet = mi("&#8226;") -> 'dot'
bullet = bullet -> 'dot'
dot = mo("&#775;") -> 'dot'
dot = mi("&#183;") -> 'dot'
perpendicular = mi("&#8869;") -> 'up tack'
square = mi("&#9633;") -> 'white square'

// Logic
and = mo("&#8743;") -> 'wedge (and)'
because = mo("&#8757;") -> 'upside-down triple dot'
compliment = mi("&#8705;") -> 'is a compliment of'
containsNormalSubgroup = mi("&#8883;") -> 'contains as normal subgroup'
exists = mo("&#8707;") -> 'Backwards E,'
factorial = mo("!") -> 'exclamation point'
forAll = mo("&#8704;") -> 'Upside-down A'
greaterThan = mo("&gt;") -> 'greater than'
greaterThan = mi("&gt;") -> 'greater than'
isElement = mo("&#8712;") -> 'element sign'
isNotElement = mo("&#8713;") -> 'is not an element of'
lessThan = mo("&lt;") -> 'less than'
lessThan = mi("&lt;") -> 'less than'
not = mo("&#172;") -> 'not sign'
or = mo("&#8744;") -> 'wedge (or)'
properSubsetRight = mo("&#8834;") -> 'subset'
properSubsetLeft = mo("&#8835;") -> 'horseshoe'
therefore = mo("&#8756;") -> 'triple dot'

e [variable] = mi("e") -> '"e"'
i [variable] = mi("i") -> '"i"'
z [variable] = mi("z") -> '"z"'

bbcapR = mi("&#8477;") -> 'blackboard cap R'

// Other symbols

// ------------------------------------------------------------------------------
// Logic-based
// ------------------------------------------------------------------------------
barWedge = munder(or bar) -> 'bar wedge'
existsOne = exists factorial -> 'backwards E followed by exclamation'
intersection = munderover(intersection mrow() mrow()) -> 'intersection'
notExist = menclose(exists) -> 'backwards E with slash'
parallelBars = verticalBar verticalBar -> 'parallel bars'
quadBars = mover(identical bar) -> 'quadruple bars'
quadBars = munder(identical bar) -> 'quadruple bars'
union = munderover(union mrow() mrow()) -> 'union'

// Strikes and Slashes
import _Vocab.txt
import _BordersSlashes.txt

//---------------------------------------------------------------
// Trigonometry-Specific
//---------------------------------------------------------------
import _Trig.txt

// Products and Unions
import _Integrals.txt

//Powers and Primes
apostrophe = apostrophe -> 'prime'

import _FunctionsLimits.txt


// Syntax Quirks
doubleBarSyn = mover(mover(mrow(+) bar) bar) -> '{1} under double bar'
doubleBarSyn = munder(munder(mrow(+) bar) bar) -> '{1} over double bar'
doubleDagger = dagger dagger -> 'double dagger'

// ------------------------------------------------------------------------------
// Fraction-based
// ------------------------------------------------------------------------------
integral = msub([integral] ?) -> '{1} sub {2} of,'

// Other symbols
infinityLogic = infinity -> 'lemniscate'
infinityLogic = infinity -> 'lemniscate'

// ------------------------------------------------------------------------------
// Conditions for arrows based on over or under
// ------------------------------------------------------------------------------
overBothArrow = munder(? [arrow]) -> '{1}, over {2},'
underBothArrow = mover(? [arrow]) -> '{1}, under {2},'
overUnderArrowB = munderover(rightArrow mrow(+) mrow(+)) -> '{2}, above {1}, sub {3},'
overUnderArrowB = munderover(leftArrow mrow(+) mrow(+)) -> '{2}, above {1}, sub {3},'
overUnderArrowB = munderover(bothArrow mrow(+) mrow(+)) -> '{2}, above {1}, sub {3},'
overUnderArrowB = munderover(rightHarpoon mrow(+) mrow(+)) -> '{2}, above {1}, sub {3},'

// ------------------------------------------------------------------------------
// Under-Over, Integrals, Products, and Summations
// ------------------------------------------------------------------------------
backPrimes = mmultiscripts(? ? ? ?) -> '{4} {1}'

// Limits
limit = munderover(mrow(l i m) mrow(? ? ?) ?) -> 'the limit as {4}, approaches {6}, of'
limit = munderover(mrow(l i m) mrow(+) ?) -> 'the limit as {4}, of'

// ------------------------------------------------------------------------------
// Parenthetical Expressions
// ------------------------------------------------------------------------------
import _Roots.txt

// ------------------------------------------------------------------------------
// Matrices
// ------------------------------------------------------------------------------

// ------------------------------------------------------------------------------
// Generic Fallbacks, Final Collectors
// ------------------------------------------------------------------------------
import _SoftFractions.txt
import _Dollars.txt
import _Fractions.txt
import _Fences.txt
import _Final.txt